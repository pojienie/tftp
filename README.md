tftp server and client

prerequisite:

    Rust (programming language)

build steps:

    cargo build

usage:

to run a client

    cargo run client <ip> (port)

to run a server

    cargo run server <directory> <ip> (port)

