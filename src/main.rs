extern crate tftp;

use std::fs::File;
use std::io;
use std::io::{Write, BufRead};
use std::io::BufReader;
use std::env;
use std::net::{ToSocketAddrs};
use tftp::server::Server;
use tftp::client::Client;
use tftp::package::Mode;

fn print_usage(prog_name:&String){
    println!("usage:");
    println!("{} server dir ip [port]: run server", prog_name);
    println!("{} client ip [port]: run client", prog_name);
}

fn print_server_usage(prog_name:&String){
    println!("usage:");
    println!("{} server dir ip [port]: run server", prog_name);
    println!("   dir: the directory of tftp root");
    println!("   ip: the ip address you want to bind");
    println!("   port: the port you want to bind (optional)");
}

fn print_client_usage(prog_name:&String){
    println!("usage:");
    println!("{} client ip [port]: run client", prog_name);
    println!("   ip: the ip address you want to bind");
    println!("   port: the port you want to bind (optional)");
}

fn server(args:&Vec<String>){
    let prog_name = &args[0];

    let dir = match args.get(2) {
        Some(v) => v,
        None => {
            println!("error: directory is not given");
            print_server_usage(prog_name);
            return;
        }
    };
    let dir = dir.to_owned();
    let dir = String::from(dir);

    let ip = match args.get(3) {
        Some(v) => v,
        None => {
            println!("error: ip is not given");
            print_server_usage(prog_name);
            return;
        }
    };

    let port = String::from("69");
    let port = args.get(4).unwrap_or(&port);

    let addr = format!("{}:{}", ip, port);
    let addr = match addr.to_socket_addrs() {
        Ok(mut v) => match v.next() {
            Some(v) => v,
            None => {
                println!("v.next() error");
                return;
            }
        },
        Err(err) => {
            println!("to socket addrs error:{}", err);
            return;
        }
    };

    let server = match Server::create(dir, addr) {
        Ok(v) => v,
        Err(err) => {
            println!("create server error: {}", err);
            return
        }
    };

    match server.start_listening() {
        Ok(v) => v,
        Err(err) => {
            println!("start listen error: {}", err);
            return
        }
    }
}

fn make_rrq_request(client:&Client, mode:Mode, src_filename:&String, target_filename:&String) {
    let data = match client.read_request(src_filename, mode) {
        Ok(v) => v,
        Err(err) => {
            println!("read request error: {}", err);
            return;
        }
    };

    let mut file = match File::create(target_filename) {
        Ok(v) => v,
        Err(err) => {
            println!("create file {} error: {}", target_filename, err);
            return;
        }
    };

    match file.write_all(&data[..]) {
        Ok(v) => v,
        Err(err) => {
            println!("write file error: {}", err);
            return;
        }
    }
}

fn make_wrq_request(client:&Client, mode:Mode, src_filename:&String, target_filename:&String) {
    let f = match File::open(src_filename) {
        Ok(v) => v,
        Err(err) => {
            println!("open file {} failed {}", src_filename, err);
            return;
        }
    };

    let reader = BufReader::new(f);

    match client.write_request(target_filename, mode, reader) {
        Ok(v) => v,
        Err(err) => {
            println!("write request error: {}", err);
            return;
        }
    };
}

fn client(args:&Vec<String>){
    let prog_name = &args[0];

    let ip = match args.get(2) {
        Some(v) => v,
        None => {
            println!("error: ip is not given");
            print_client_usage(prog_name);
            return;
        }
    };

    let port = String::from("69");
    let port = args.get(3).unwrap_or(&port);

    let addr = format!("{}:{}", ip, port);
    let addr = match addr.to_socket_addrs() {
        Ok(mut v) => match v.next() {
            Some(v) => v,
            None => {
                println!("v.next() error");
                return;
            }
        },
        Err(err) => {
            println!("to socket addrs error:{}", err);
            return;
        }
    };

    let client = match Client::connect(addr) {
        Ok(v) => v,
        Err(err) => {
            println!("connect error: {}", err);
            return;
        }
    };

    let stdin = io::stdin();
    let mut stdout = io::stdout();
    let mut iter = stdin.lock().lines();
    loop {
        print!(">");
        //stdout.flush().ok().expect("Could not flush stdout");
        match stdout.flush() {
            Ok(v) => v,
            Err(err) => {
                println!("flush stdout error: {}", err);
                return
            }
        };
        let line = match iter.next() {
            Some(v) => v,
            None => return
        };
        let line = match line {
            Ok(v) => v,
            Err(err) => {
                println!("read line error: {}", err);
                return
            }
        };

        let line = String::from(line);
        let split = line.split(" ");
        let split:Vec<&str> = split.collect();

        let operation = match split.get(0){
            Some(v) => v,
            None => continue
        };

        let src_filename = match split.get(1){
            Some(v) => v,
            None => {
                println!("source filename not given");
                continue;
            },
        };

        let target_filename = match split.get(2){
            Some(v) => v,
            None => {
                println!("target filename not given");
                continue;
            },
        };

        let src_filename = String::from(*src_filename);
        let target_filename = String::from(*target_filename);
        match operation {
            &"get" => make_rrq_request(&client, Mode::Octet, &src_filename, &target_filename),
            &"put" => make_wrq_request(&client, Mode::Octet, &src_filename, &target_filename),
            _      => {
                println!("unknown operation: {}", operation)
            }
        };
    }
}

fn main() {
    let args:Vec<String> = env::args().collect();

    let prog_name = &args[0];

    let command = match args.get(1) {
        Some(v) => v,
        None => {
            println!("error: command is not given");
            print_usage(prog_name);
            return;
        }
    };

    match &command[..] {
        "server" => server(&args),
        "client" => client(&args),
        _ => {
            println!("invalid command: {}", command);
            print_usage(prog_name);
        }
    }
}
