use std::net::UdpSocket;
use std::net::{SocketAddr};
use std::io::Result;
use std::io::{Error, ErrorKind};
use std::io::{BufRead};
use std::time::Duration;

use package::{Package};
use binary::Binary;

pub fn send_until_recv(socket:&UdpSocket, addr:SocketAddr, package:Package) -> Result <(Package, SocketAddr)>{
    const TIMEOUT:Duration = Duration::from_secs(2);
    const RETRY:u8 = 3;

    let package = package.serialize();
    let backup_read_timeout = socket.read_timeout()?;

    let mut buf = [0;516]; // opcode = 2, block_number = 2, data = 512

    for _ in [0..RETRY].iter() {
        socket.send_to(&package, addr)?;

        socket.set_read_timeout(Some(TIMEOUT))?;
        let data = socket.recv_from(&mut buf);
        socket.set_read_timeout(backup_read_timeout)?;
        let (size, src_addr) = match data {
            Ok(v) => v,
            Err(err) => {
                println!("retrying: {}", err);
                continue
            }
        };

        let buf = buf[..size].to_vec();
        let (_size, ret_package) = Binary::deserialize(&buf)?;
        socket.set_read_timeout(backup_read_timeout)?;
        return Ok((ret_package, src_addr));
    }

    let s = format!("send timeout");
    send_error_package(socket, addr, 0, &s)?;
    return Err(Error::new(ErrorKind::Other, s))
}

pub fn send_error_package(socket:&UdpSocket, addr:SocketAddr, err_code:u16, err_msg:&String) -> Result <()> {
    let err_msg = err_msg.to_owned();
    let err_msg_copy = err_msg.clone();
    let package = Package::Error {
        err_code,
        err_msg,
    };

    let err_msg = err_msg_copy;

    let package = package.serialize();
    let _size = socket.send_to(&package[..], addr)?;

    Err(Error::new(ErrorKind::Other, err_msg))
}

pub fn send_data_package<T:BufRead>(socket:&UdpSocket, addr:SocketAddr, mut content:T) -> Result<()> {
    let mut block_number:u16 = 0;
    let mut buf = [0;512];

    let mut resent = false;
    let mut size = 0;
    let mut package = Package::Error {
        err_code:0,
        err_msg:String::from("won't happen")
    };
    loop {
        if !resent {
            size = content.read(&mut buf)?;

            let data = buf[..size].to_vec();

            package = Package::Data {
                block_number: block_number.wrapping_add(1),
                data: data
            };
        }

        let package = package.clone();
        let (ret_package, src_addr) = send_until_recv(socket, addr, package)?;

        match ret_package {
            // ACK package
            Package::Ack{block_number:ack_block_number} => {
                if block_number.wrapping_add(1) != ack_block_number {
                    resent = true;
                    continue;
                }
                block_number = ack_block_number;
                resent = false;

                if size < 512 {
                    return Ok(());
                }
            },

            Package::Error{err_code, err_msg} => {
                let err_msg = format!("{} (code: {})", err_msg, err_code);
                return Err(Error::new(ErrorKind::Other, err_msg))
            },

            _ => {
                let s = format!("unknown package {:?}", ret_package);
                send_error_package(socket, src_addr, 0, &s)?;
                return Err(Error::new(ErrorKind::Other, s))
            }
        }
    }
}

pub fn receive_data_package(socket:&UdpSocket, addr:SocketAddr, first_package:Package) -> Result<Vec<u8>> {
    let mut prev_block_number:u16 = 0;
    let mut acc_data = Vec::new();
    let (mut ret_package, mut src_addr) = send_until_recv(socket, addr, first_package)?;
    loop {
        match ret_package {
            Package::Data{block_number, mut data} => {
                let ack_package = Package::Ack{
                    block_number
                };

                if block_number != prev_block_number.wrapping_add(1) {
                    //either package dupe or ack missing, resend ack to be safe
                    let (new_ret_package, new_src_addr) = send_until_recv(socket, src_addr, ack_package)?;
                    ret_package = new_ret_package;
                    src_addr = new_src_addr;
                    continue;
                }
                prev_block_number = block_number;

                let size = data.len();
                acc_data.append(&mut data);
                if size < 512 {
                    let package = ack_package.serialize();
                    socket.send_to(&package, src_addr)?;
                    return Ok(acc_data);
                }

                let (new_ret_package, new_src_addr) = send_until_recv(socket, src_addr, ack_package)?;
                ret_package = new_ret_package;
                src_addr = new_src_addr;
                continue;
            },

            Package::Error{err_code, err_msg} => {
                let err_msg = format!("{} (code: {})", err_msg, err_code);
                return Err(Error::new(ErrorKind::Other, err_msg))
            },

            _ => {
                let s = format!("unknown package {:?}", ret_package);
                send_error_package(socket, src_addr, 0, &s)?;
                return Err(Error::new(ErrorKind::Other, s))
            }
        }
    }
}
