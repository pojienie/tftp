///implemented according to rfc1350 https://tools.ietf.org/html/rfc1350
use std::net::UdpSocket;
use std::net::{SocketAddr};
use std::io::Result;
use std::io::{Error, ErrorKind};
use std::io::{BufRead};

use package::{Package, Mode};
use util::{send_data_package, send_error_package, receive_data_package, send_until_recv};

#[derive(Debug)]
pub struct Client{
    socket:UdpSocket,
    addr:SocketAddr
}

impl Client {
    pub fn connect(addr:SocketAddr) -> Result<Client> {
        let socket = UdpSocket::bind("0.0.0.0:0")?;

        let client = Client{
            socket:socket,
            addr:addr
        };

        Ok(client)
    }

    pub fn read_request(&self, filename:&String, mode:Mode) -> Result<Vec<u8>> {
        let filename = filename.to_owned();
        let package = Package::Rrq{
            filename,
            mode
        };
        receive_data_package(&self.socket, self.addr, package)
    }

    pub fn write_request<T:BufRead>(&self, filename:&String, mode:Mode, content:T) -> Result<()> {
        let filename = filename.to_owned();
        let package = Package::Wrq{
            filename,
            mode,
        };

        let (ret_package, src_addr) = send_until_recv(&self.socket, self.addr, package)?;
        match ret_package {
            // ACK package
            Package::Ack{block_number:_} => {
                send_data_package(&self.socket, src_addr, content)?;
                return Ok(())
            },

            Package::Error{err_code, err_msg} => {
                let err_msg = format!("{} (code: {})", err_msg, err_code);
                return Err(Error::new(ErrorKind::Other, err_msg))
            },

            _ => {
                let s = format!("unknown package {:?}", ret_package);
                send_error_package(&self.socket, src_addr, 0, &s)?;
                return Err(Error::new(ErrorKind::Other, s))
            }
        }
    }
}
