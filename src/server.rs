///implemented according to rfc1350 https://tools.ietf.org/html/rfc1350
use std::net::UdpSocket;
use std::net::{SocketAddr};
use std::io::Result;
use std::io::{Error, ErrorKind};
use std::fs::File;
use std::io::{Write, BufReader};

use package::{Package, Mode};
use util::{send_error_package, send_data_package, receive_data_package};
use binary::Binary;

#[derive(Debug)]
pub struct Server{
    socket:UdpSocket,
    dir:String,
    addr:SocketAddr
}

impl Server {
    pub fn create(dir:String, addr:SocketAddr) -> Result<Server> {
        let socket = UdpSocket::bind(addr)?;

        let server = Server{
            socket,
            dir,
            addr
        };

        Ok(server)
    }

    fn check_path(dir:&String) -> bool {
        // avoid accessing root
        let first_char = match dir.chars().next() {
            Some(v) => v,
            None => '/'
        };

        if first_char == '\\' || first_char == '/' {
            return false;
        }

        // avoid parent directory
        let mut prev_c = first_char;
        for c in dir.chars().skip(1) {
            if c == '.' && prev_c == '.' {
                return false
            }
            prev_c = c;
        }

        true
    }

    fn process_wrq(&self, addr:SocketAddr, filename:&String, _mode:Mode) -> Result<()> {
        if !Server::check_path(filename) {
            let s = format!("invalid path: {}", filename);
            send_error_package(&self.socket, addr, 0, &s)?;
            return Err(Error::new(ErrorKind::Other, s));
        }

        let mut full_path = self.dir.clone();
        full_path.push_str(filename);

        let mut f = match File::create(full_path) {
            Ok(v) => v,
            Err(err) => {
                let s = format!("create file {} failed: {}", filename, err);
                send_error_package(&self.socket, addr, 0, &s)?;
                return Err(Error::new(ErrorKind::Other, s));
            }
        };

        let ack_package = Package::Ack{
            block_number:0
        };
        let content = receive_data_package(&self.socket, addr, ack_package)?;

        match f.write_all(&content) {
            Ok(v) => v,
            Err(err) => {
                let s = format!("write file {} failed: {}", filename, err);
                send_error_package(&self.socket, addr, 0, &s)?;
                return Err(Error::new(ErrorKind::Other, s));
            }
        }

        Ok(())
    }

    fn process_rrq(&self, addr:SocketAddr, filename:&String, _mode:Mode) -> Result<()> {
        if !Server::check_path(filename) {
            let s = format!("invalid path: {}", filename);
            send_error_package(&self.socket, addr, 0, &s)?;
            return Err(Error::new(ErrorKind::Other, s));
        }

        let mut full_path = self.dir.clone();
        full_path.push_str(filename);

        let f = match File::open(full_path) {
            Ok(v) => v,
            Err(err) => {
                let s = format!("open file {} failed: {}", filename, err);
                send_error_package(&self.socket, addr, 0, &s)?;
                return Err(Error::new(ErrorKind::Other, s));
            }
        };

        let reader = BufReader::new(f);

        send_data_package(&self.socket, addr, reader)
    }

    pub fn start_listening(&self) -> Result<()> {
        let mut buf = [0; 516]; //opcode = 2, block# = 2, data=512

        loop {
            let (size, src_addr) = match self.socket.recv_from(&mut buf) {
                Ok(v) => v,
                Err(err) => {
                    println!("error recv_from: {}", err);
                    continue;
                }
            };
            let buf = buf[..size].to_vec();

            let (_size, ret_package) = match Binary::deserialize(&buf) {
                Ok(v) => v,
                Err(err) => {
                    println!("error deserializing package {}", err);
                    continue;
                }
            };

            match ret_package {
                Package::Wrq{filename, mode} => {
                    match self.process_wrq(src_addr, &filename, mode) {
                        Ok(_) => continue,
                        Err(err) => {
                            println!("error processing wrq: {}", err);
                            continue;
                        }
                    }
                },

                Package::Rrq{filename, mode} => {
                    match self.process_rrq(src_addr, &filename, mode) {
                        Ok(_) => continue,
                        Err(err) => {
                            println!("error processing rrq: {}", err);
                            continue;
                        }
                    }
                },

                _ => {
                    println!("received {:?}", ret_package);
                    let s = format!("unknown or invalid opcode: {:?}", ret_package);
                    match send_error_package(&self.socket, src_addr, 0, &s) {
                        Ok(_) => continue,
                        Err(err) => {
                            println!("error sending error package: {}", err);
                            continue;
                        }
                    }
                }
            }
        }
    }
}
