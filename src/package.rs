use std::io::Result;
use std::io::{Error, ErrorKind};

use binary::Binary;

#[derive(Debug, Copy, Clone)]
pub enum Mode {
    Netascii,
    Octet,
    Mail
}

#[derive(Debug, Clone)]
pub enum Package {
    Rrq{
        filename:String,
        mode:Mode,
    },
    Wrq{
        filename:String,
        mode:Mode,
    },
    Data{
        block_number:u16,
        data:Vec<u8>
    },
    Ack{
        block_number:u16,
    },
    Error{
        err_code:u16,
        err_msg:String
    }
}

impl Package {
    fn mode_to_string(mode:&Mode) -> String {
        let mode = match mode {
            Mode::Netascii => "netascii",
            Mode::Octet => "octet",
            Mode::Mail => "mail"
        };
        String::from(mode)
    }

    fn string_to_mode(mode:&String) -> Result<Mode> {
        match &mode[..] {
            "netascii" => Ok(Mode::Netascii),
            "octet" => Ok(Mode::Octet),
            "mail" => Ok(Mode::Mail),
            _ => {
                let s = format!("unknown mode: {}", mode);
                return Err(Error::new(ErrorKind::Other, s));
            }
        }
    }
}

impl Binary for Package {
    fn serialize(&self) -> Vec<u8> {
        match self {
            Package::Rrq{filename, mode} => {
                // opcode
                let mut package = vec![0, 1];

                // filename
                let mut filename = filename.as_bytes().to_vec();
                package.append(&mut filename);

                // zero
                package.push(0);

                // mode
                let mode = Package::mode_to_string(&mode);
                let mut mode = mode.as_bytes().to_vec();
                package.append(&mut mode);

                // zero
                package.push(0);

                package
            },

            Package::Wrq{filename, mode} => {
                // opcode
                let mut package = vec![0, 2];

                // filename
                let mut filename = filename.as_bytes().to_vec();
                package.append(&mut filename);

                // zero
                package.push(0);

                // mode
                let mode = Package::mode_to_string(&mode);
                let mut mode = mode.as_bytes().to_vec();
                package.append(&mut mode);

                // zero
                package.push(0);

                package
            },

            Package::Data{block_number, data} => {
                // opcode
                let mut package = vec![0, 3];

                // block number
                let high = (block_number >> 8) as u8;
                let low = (block_number & 0xFF) as u8;
                package.push(high);
                package.push(low);

                // data
                let mut data = data.clone();

                package.append(&mut data);

                package
            },

            Package::Ack{block_number} => {
                let high = (block_number >> 8) as u8;
                let low = (block_number & 0xFF) as u8;

                let package = vec![0,4,high,low];

                package
            }

            Package::Error{err_code, err_msg} => {
                let high = (err_code >> 8) as u8;
                let low = (err_code & 0xFF) as u8;

                let mut package = vec![0,5,high,low];

                let mut err_msg = err_msg.as_bytes().to_vec();
                package.append(&mut err_msg);

                package.push(0);

                package
            }
        }
    }

    fn deserialize(data:&Vec<u8>) -> Result<(usize, Package)> {
        if data.len() < 2 {
            let s = "server didn't send op code";
            return Err(Error::new(ErrorKind::Other, s));
        }

        let opcode:u16 = ((data[0] as u16) << 8) + (data[1] as u16);
        match opcode {
            // RRQ package
            1 | 2 => {
                let mut mode = Vec::new();
                let mut filename = Vec::new();

                let mut p = 2;
                for (i, item) in data[p..].iter().enumerate() {
                    if *item == 0 {
                        p += i + 1;
                        break;
                    }
                    filename.push(*item);
                }
                let filename = String::from_utf8_lossy(&filename[..]);
                let filename = filename.to_string();

                for (i, item) in data[p..].iter().enumerate() {
                    if *item == 0 {
                        p += i + 1;
                        break;
                    }
                    mode.push(*item);
                }
                let mode = String::from_utf8_lossy(&mode[..]);
                let mode = mode.to_string();
                let mode = Package::string_to_mode(&mode)?;

                let package;

                if opcode == 1 {
                    package = Package::Rrq {
                        filename,
                        mode
                    };
                } else {
                    package = Package::Wrq {
                        filename,
                        mode
                    };
                }

                Ok((p, package))
            },

            // DATA package
            3 => {
                let len = data.len();
                if len < 4 {
                    let s = "server didn't send block number in data package";
                    return Err(Error::new(ErrorKind::Other, s));
                }
                let block_number:u16 = ((data[2] as u16) << 8) + (data[3] as u16);
                let data = &data[4..];
                let data = data.to_vec();

                let package = Package::Data{
                    block_number,
                    data,
                };

                Ok((len, package))
            },

            // ACK
            4 => {
                if data.len() < 4 {
                    let s = "server didn't send block number in ack package";
                    return Err(Error::new(ErrorKind::Other, s));
                }
                let block_number:u16 = ((data[2] as u16) << 8) + (data[3] as u16);
                let package = Package::Ack{
                    block_number
                };

                Ok((4, package))
            },

            // ERROR
            5 => {
                let len = data.len();
                if len < 4 {
                    let s = "server didn't send error code in error package";
                    return Err(Error::new(ErrorKind::Other, s));
                } else if len < 5 {
                    let s = "server didn't end error package with 0";
                    return Err(Error::new(ErrorKind::Other, s));
                } else if data[len-1] != 0 {
                    let s = "the last byte of error package isn't 0";
                    return Err(Error::new(ErrorKind::Other, s));
                }

                let err_code:u16 = ((data[2] as u16) << 8) + (data[3] as u16);
                let err_msg = &data[4..len - 1];
                let err_msg = String::from_utf8_lossy(err_msg);
                let err_msg = err_msg.to_string();

                let package = Package::Error{
                    err_code,
                    err_msg
                };
                Ok((len, package))
            },

            _ => {
                let s = format!("unknown opcode {}", opcode);
                return Err(Error::new(ErrorKind::Other, s));
            }
        }

    }
}
