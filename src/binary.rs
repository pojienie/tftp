use std::io::Result;

pub trait Binary: Sized {
    fn serialize(&self) -> Vec<u8>;
    fn deserialize(data:&Vec<u8>) -> Result<(usize, Self)>;
}
